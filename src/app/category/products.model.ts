import { Images } from './images.model';

export class Products {
  public id: number;
  public name: string;
  public link: string;
  public code: string;
  public active: boolean;
  public images: Images[];

  constructor(id: number, name: string, link: string, code: string, active: boolean, images: Images[]) {
    this.id = id;
    this.name = name;
    this.link = link;
    this.code = code;
    this.active = active;
    this.images = images;
  }
}
