export class Images {
  public type: Types[];
  public name: string;
  public path: string;
}

export class Types {
  public mobile: string;
  public destop: string;
}
