export interface IPCategories {
  categories: ICategories[];
  products: IProducts[];
}

export interface ICategories {
  id: number;
  name: string;
  link: string;
  keywords: string;
  active: boolean;
  parentId: number;
}

export interface IProducts {
  id: number;
  name: string;
  link: string;
  code: string;
  active: boolean;
}
