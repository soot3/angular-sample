import { Images } from './images.model';

export class Category {
  public id: number;
  public name: string;
  public link: string;
  public keywords: string;
  public active: boolean;
  public parentId: number;
  public images: Images[];

  constructor(id: number, name: string, link: string, keywords: string, active: boolean, parentId: number, images: Images[]) {
    this.id = id;
    this.name = name;
    this.link = link;
    this.keywords = keywords;
    this.active = active;
    this.parentId = parentId;
    this.images = images;
  }
}
