import { Component, OnInit } from '@angular/core';
import { CategoryService } from './category.service';
import { Category } from './category.model';
import { Products } from './products.model';
import { ActivatedRoute, Params } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { ProductService } from '../product/product.service';
import { TranslateService } from '@ngx-translate/core';
import { BreadcrumbsService } from 'ng6-breadcrumbs';
import { environment } from '../../environments/environment.prod';

declare var $: any;

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  link: string;
  id: number;
  country: string;
  language: string;

  categoryName: string;
  product: any;

  categories: Category[] = [];
  products: Products[] = [];

  public enableFutureLink = false;

  constructor(
    private categoryService: CategoryService,
    private route: ActivatedRoute,
    private titleService: Title,
    private productService: ProductService,
    private translate: TranslateService,
    private breadcrumbsService: BreadcrumbsService
  ) {}

  getCategory(id: number, country: string, language: string): void {
    this.categoryService.getCategory(id, country, language)
      .subscribe(
        resultArray => {
          this.categories = resultArray;
          this.enableFutureLink = true;
        },
        error => console.log('Error ::' + error)
      );
  }

  getProducts(id: number, country: string, language: string): void {
    this.categoryService.getProducts(id, country, language)
      .subscribe(
        resultArray => this.products = resultArray,
        error => console.log('Error ::' + error)
      );
  }

  getProduct(id: number, country: string, language: string): void {
    this.productService.getProduct(id, country, language)
      .subscribe(
        resultArray => this.product = resultArray,
        error => console.log('Error ::' + error)
      );
  }

  setTitle(id: number, country: string, language: string) {

    this.categoryService.getCategory(id, country, language)
      .subscribe(
        resultArray => {
          this.categories = resultArray;
          this.categoryName = this.categories[0].name;


          const homeUrl = '/' + country + '/' + language;
          this.translate.get(['MAINMENUTVS', 'HITACHIHOME'])
            .subscribe(
              translations => {
                this.breadcrumbsService.store([
                  { label: translations.HITACHIHOME, url: homeUrl, params: [] },
                  { label: translations.MAINMENUTVS, url: homeUrl + '/tvs', params: [] },
                  { label: this.categoryName, url: '', params: [] }
                ]);

                this.titleService.setTitle(this.categoryName + ' | ' + environment.siteTitle);
              });
        },
        error => console.log('Error ::' + error)
      );

  }

  // getProducts(): void {

  // }

  ngOnInit(): void {

    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.link = params['link'];
          this.country = params['country'];
          this.language = params['language'];
          this.getProducts(this.id, this.country, this.language);
          this.getCategory(this.id, this.country, this.language);
          this.setTitle(this.id, this.country, this.language);
          this.getProduct(this.id, this.country, this.language);
        }
      );

    $(function () {
      $('a.products').addClass('is-active');
    });

  }

}
