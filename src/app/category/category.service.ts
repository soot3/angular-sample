import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import 'rxjs/Rx';

import { Category } from './category.model';
import { environment } from '../../environments/environment.prod';

@Injectable()
export class CategoryService {


  constructor(
    private http: HttpClient
  ) {}

  categoriesUrl = environment.rootUrl + 'tr/tr/category';
  productsUrl = environment.rootUrl + 'tr/tr/category';

  getCategories(country: string, language: string) {
    return this.http
      .get<Category[]>(environment.rootUrl + country + '/' + language + '/category')
      .map(
        (categories) => {
          for (const category of categories) {
            category['categories'] = [];
          }
          return categories;
        });
  }

  getCategory(id: number, country: string, language: string) {
    return this.http
      .get(environment.rootUrl + country + '/' + language + '/category/' + id)
      .map(
        (categories) => {
          return categories['categories'];

        });
  }

  getProducts(id: number, country: string, language: string) {
    return this.http
      .get(environment.rootUrl + country + '/' + language + '/category/' + id)
      .map(
        (products) => {
          return products['products'];
        },
        error => console.log('Error ::' + error)
      );
  }

  getProductsHome(country: string, language: string) {
    return this.http
      .get(environment.rootUrl + country + '/' + language + '/product')
      .map(
        (product) => {
          return product;
        },
        error => console.log('Error ::' + error)
      );
  }

  getAll(country: string, language: string) {
    return this.http
      .get(environment.rootUrl + country + '/' + language + '/category')
      .map(
        (category) => {
          return category;
        });
  }

}
