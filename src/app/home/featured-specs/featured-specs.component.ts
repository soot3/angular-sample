import { Component, OnInit } from '@angular/core';
import { FeaturedSpecsService } from './featuredSpecs.service';
import { ActivatedRoute } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-featured-specs',
  templateUrl: './featured-specs.component.html',
  styleUrls: ['./featured-specs.component.scss']
})
export class FeaturedSpecsComponent implements OnInit {

  country: string;
  language: string;
  featuredSpecs: any;

  constructor(private featuredSpecsService: FeaturedSpecsService, private route: ActivatedRoute) { }

  getFeaturedSpecs(country: string, language: string): void {
    this.featuredSpecsService.getFeaturedSpecs(country, language)
      .subscribe(
        resultArray => {
          this.featuredSpecs = resultArray;
        },
        error => console.log('Error ::' + error)
      );
  }

  changeTab(dataTab: string) {
    $('ul.spec-titles .tabs-title').removeClass('is-active');
    $('.tabs-content.spec-tabs .tabs-panel').removeClass('is-active');

    $('#title' + dataTab).addClass('is-active');
    $('#tab' + dataTab).addClass('is-active');
  }

  ngOnInit() {
    setTimeout(() => {
      const pathname = window.location.pathname;
      const pathnameSplit = pathname.split('/');
      this.country = pathnameSplit[1];
      this.language = pathnameSplit[2];
      this.getFeaturedSpecs(this.country, this.language);
    }, 1500);

  }

}
