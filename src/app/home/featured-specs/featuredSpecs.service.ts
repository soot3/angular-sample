import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Injectable()
export class FeaturedSpecsService {

    constructor(private http: HttpClient) {}
 
    getFeaturedSpecs(country: string, language: string) {
        return this.http
        .get(environment.rootUrl + country + '/' + language + '/featuredSpec')
        .map(
        (featuredSpecs) => {
            return featuredSpecs;
        });
    }
}
