import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from '../../category/category.service';

@Component({
  selector: 'app-home-product',
  templateUrl: './home-product.component.html',
  styleUrls: ['./home-product.component.scss']
})
export class HomeProductComponent implements OnInit {
  
  country: string;
  language: string;
  homeProduct : any;
  productName: string;
  productSize: string;
  inches32= [];
  inches40= [];
  inches49= [];
  inches65= [];
  
  constructor(private categoryService: CategoryService) { }
  
  getProductsHome(country: string, language: string): void {
    this.categoryService.getProductsHome(country, language)
      .subscribe(
        resultArray => {
          
          this.homeProduct = resultArray
          for (let product of this.homeProduct) {
            if(product.size < 40) {
              this.inches32.push(product);
            }else if(product.size < 49) {
              this.inches40.push(product);
            }else if(product.size < 65) {
              this.inches49.push(product);
            }else {
              this.inches65.push(product);
            }
          }
        },
        error => console.log('Error ::' + error)
      )
  }
            

  ngOnInit() {

    setTimeout(()=> {
      let pathname = window.location.pathname;
      let pathnameSplit = pathname.split('/');
      this.country = pathnameSplit[1];
      this.language = pathnameSplit[2];
      this.getProductsHome(this.country, this.language);
    }, 1500);


    // this.route.parent.params
    //   .subscribe(
    //     (params: Params) => {
    //       this.country = params['country'];
    //       this.language = params['language'];
    //       this.getProductsHome(this.country, this.language);
    //     }
    //   )
  }

}
