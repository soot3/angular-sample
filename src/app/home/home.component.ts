import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from 'src/environments/environment.prod';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  subscription: Subscription;
  country: string;
  language: string;

  constructor(private titleService: Title, private route: ActivatedRoute) { }

  ngOnInit() {
    this.titleService.setTitle(environment.siteTitle);
    this.route.params
    .subscribe(
      (params: Params) => {
        this.country = params['country'];
        this.language = params['language'];
      }
    );

  }

}

