import { Component, OnInit } from '@angular/core';
import { FeaturedProductService } from './featuredProduct.service';
import { ActivatedRoute } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-featured-product',
  templateUrl: './featured-product.component.html',
  styleUrls: ['./featured-product.component.scss']
})
export class FeaturedProductComponent implements OnInit {
 
  country: string;
  language: string;
  featuredSpecs: any;

  constructor(private featuredSpecsService: FeaturedProductService, private route: ActivatedRoute) { }

  getFeatured(country: string, language: string): void {
    this.featuredSpecsService.getFeatured(country, language)
      .subscribe(
        resultArray => {
          this.featuredSpecs = resultArray;
          console.log(this.featuredSpecs);
        },
        error => console.log('Error ::' + error)
      )
  }
  
  ngOnInit() {
    setTimeout(()=> {
      let pathname = window.location.pathname;
      let pathnameSplit = pathname.split('/');
      this.country = pathnameSplit[1];
      this.language = pathnameSplit[2];
      this.getFeatured(this.country, this.language);

      $(document).ready(function(){
	
        $('ul.tabs li[_ngcontent-c9]').click(function(){
          var tab_id = $(this).attr('data-tab');
      
          $('ul.tabs li[_ngcontent-c9]').removeClass('is-active');
          $('.tabs-content .tabs-panel').removeClass('is-active');
      
          $(this).addClass('is-active');
          $("#"+tab_id).addClass('is-active');
        })
      
      });
    }, 1500);

    
  }

}
