import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { environment } from 'src/environments/environment';

@Injectable()
export class FeaturedProductService {

    constructor(private http: HttpClient) {}

    getFeatured(country: string, language: string) {
        return this.http
        .get(environment.rootUrl + country + '/' + language + '/featuredCategory/')
        .map(
        (fcategory) => {
            return fcategory;
        });
    }
}
