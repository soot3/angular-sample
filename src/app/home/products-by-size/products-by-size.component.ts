import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../../category/category.service';
declare var $: any;

@Component({
  selector: 'app-products-by-size',
  templateUrl: './products-by-size.component.html',
  styleUrls: ['./products-by-size.component.scss']
})
export class ProductsBySizeComponent implements OnInit {

  country: string;
  language: string;
  homeProduct : any;
  productName: string;
  productSize: string;
  inches32= [];
  inches40= [];
  inches49= [];
  inches65= [];

  constructor(private categoryService: CategoryService) { }
  
  getProductsHome(country: string, language: string): void {
    this.categoryService.getProductsHome(country, language)
      .subscribe(
        resultArray => {
          
          this.homeProduct = resultArray
          for (let product of this.homeProduct) {
            if(product.size < 40) {
              this.inches32.push(product);
            }else if(product.size < 49) {
              this.inches40.push(product);
            }else if(product.size < 65) {
              this.inches49.push(product);
            }else {
              this.inches65.push(product);
            }
          }
        },
        error => console.log('Error ::' + error)
      )
  }

  changeTab(dataTab: string){
    let tab_id = dataTab;

    $(".discover-sizes ul #inchtitle" + tab_id).siblings().removeClass('is-active');
    $('.tabs-content.inchtabs .tabs-panel').removeClass('is-active');

    $("#inchtitle" + tab_id).addClass('is-active');
    $("#inch"+tab_id).addClass('is-active');
  }
            

  ngOnInit() {
    // $(document).foundation();
    setTimeout(()=> {
      let pathname = window.location.pathname;
      let pathnameSplit = pathname.split('/');
      this.country = pathnameSplit[1];
      this.language = pathnameSplit[2];
      this.getProductsHome(this.country, this.language);
    }, 1500);


    // this.route.parent.params
    //   .subscribe(
    //     (params: Params) => {
    //       this.country = params['country'];
    //       this.language = params['language'];
    //       this.getProductsHome(this.country, this.language);
    //     }
    //   )
  }

}
