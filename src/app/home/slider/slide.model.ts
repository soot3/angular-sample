import { SlideItem } from './slide-item.model';

export class Slide {
  public id: Number;
  public active: Boolean;
  public createdAt: Date;
  public updatedAt: Date;
  public deletedAt: Date;
  public slideItem: SlideItem;

  constructor(id: Number, active: Boolean, createdAt: Date, updatedAt: Date, deletedAt: Date, slideItem: SlideItem) {
    this.id = id;
    this.active = active;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
    this.slideItem = slideItem;
  }
}