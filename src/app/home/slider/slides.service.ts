import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Slide } from './slide.model';
import { environment } from 'src/environments/environment.prod';

@Injectable()
export class SlidesService {

  constructor(private http: HttpClient) {}

  getSlides(country: string, language: string) {
    return this.http
    .get<Slide[]>(environment.rootUrl + country + '/' + language + '/slider/main')
    .map(
    (slides) => {
        return slides;
    });
  }
}
