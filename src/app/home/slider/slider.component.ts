import { Component, OnInit, ViewChild } from '@angular/core';
import { SlidesService } from './slides.service';
import { SwiperDirective, SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { ActivatedRoute, Params } from '@angular/router';

import { Slide } from './slide.model';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  slides: Slide[];

  country: string;
  language: string;

  public show: boolean = true;

  public type: string = 'directive';

  public disabled: boolean = false;

  public config: SwiperConfigInterface = {
    a11y: true,
    direction: 'horizontal',
    slidesPerView: 1,
    keyboard: true,
    mousewheel: false,
    scrollbar: false,
    navigation: true,
    pagination: true,
    threshold: 0,
    autoplay: {
      delay: 10000
    }
  };

  // @ViewChild(SwiperComponent) componentRef?: SwiperComponent;
  @ViewChild(SwiperDirective) directiveRef?: SwiperDirective;
  
  constructor(private slidesService: SlidesService, private route: ActivatedRoute) { }

  getSlides(country: string, language: string): void {
    this.slidesService.getSlides(country, language)
      .subscribe(
        resultArray => {
          this.slides = resultArray
        },
        error => console.log('Error ::' + error)
      )
  }

  ngOnInit(): void {
    setTimeout(()=> {
      let pathname = window.location.pathname;
      let pathnameSplit = pathname.split('/');
      this.country = pathnameSplit[1];
      this.language = pathnameSplit[2];
      this.getSlides(this.country, this.language);
    }, 1500);
    // this.route.parent.params
    //   .subscribe(
    //     (params: Params) => {
    //       this.country = params['country'];
    //       this.language = params['language'];
    //       this.getSlides(this.country, this.language);
    //       console.log(this.country);
    //     }
    
    //   )
  }
  
}