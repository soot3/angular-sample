export class SlideItem {
  public id: Number;
  public path: String;
  public mimeType: String;
  public active: Boolean;
  public priority: Number;
  public createdAt: Date;
  public updatedAt: Date;
  public deletedAt: Date;

  constructor(
    id: Number, 
    path: String, 
    mimeType: String, 
    active: boolean, 
    priority: number, 
    createdAt: Date, 
    updatedAt: Date, 
    deletedAt: Date
  ){
    this.id = id;
    this.path = path;
    this.mimeType = mimeType;
    this.active = active;
    this.priority = priority;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.deletedAt = deletedAt;
  }
}