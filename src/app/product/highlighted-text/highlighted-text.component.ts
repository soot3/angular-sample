import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-highlighted-text',
  templateUrl: './highlighted-text.component.html',
  styleUrls: ['./highlighted-text.component.scss']
})
export class HighlightedTextComponent implements OnInit {

  id: number;
  link: string;
  country: string;
  language: string;
  highlightedTexts: any = [];

  constructor(private productService: ProductService, private route: ActivatedRoute) { }

  getHighlightedText(id: number, country: string, language: string): void {
    this.productService.getHighlightedText(id, country, language)
      .subscribe(
        resultArray => this.highlightedTexts = resultArray,
        error => console.log('Error ::' + error)
    );
  }

  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.link = params['link'];
          this.country = params['country'];
          this.language = params['language'];
          this.getHighlightedText(this.id, this.country, this.language);
      }
    );
  }
}
