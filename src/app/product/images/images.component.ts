import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductService } from '../../product/product.service';

declare var $: any;

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.scss']
})
export class ImagesComponent implements OnInit {

  link: string;
  id: number;
  country: string;
  language: string;

  productName: string;

  images: any;

  slideGallery = {
    'slidesToShow': 1,
    'slidesToScroll': 1,
    'arrows': false,
    'fade': true,
    'asNavFor': '.thumbnails'
  };

  slideThumbnails = {
    'slidesToShow': 3,
    'slidesToScroll': 1,
    'asNavFor': '.gallery',
    'dots': false,
    'centerMode': true,
    'centerPadding': '15px',
    'focusOnSelect': true,
    'vertical': true,
    'arrows': true,
    'swipe': true,
    'swipeToSlide': true,
    'verticalSwiping': true
  };

  constructor(private productService: ProductService, private route: ActivatedRoute) {}

  getImages(id: number, country: string, language: string): void {
    this.productService.getImages(id, country, language)
      .subscribe(
        resultArray => {
          this.images = resultArray;
        },
        error => console.log('Error ::' + error)
      );
  }

  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.link = params['link'];
          this.country = params['country'];
          this.language = params['language'];
          this.getImages(this.id, this.country, this.language);
        }
      );
   }

}
