import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { ProductService } from './product.service';
import { Title } from '@angular/platform-browser';
import { environment } from '../../environments/environment.prod';
import { TranslateService } from '@ngx-translate/core';
import { BreadcrumbsService } from 'ng6-breadcrumbs';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  link: string;
  id: number;
  country: string;
  language: string;

  productName: string;
  productSize: any;

  categoryName: string;
  categoryLink: string;
  categoryId: string;

  product: any;

  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    public titleService: Title,
    private translate: TranslateService,
    private breadcrumbsService: BreadcrumbsService
  ) { }

  getProduct(id: number, country: string, language: string): void {
    this.productService.getProduct(id, country, language)
      .subscribe(
        resultArray => {
          this.product = resultArray;
          this.productName = resultArray[0].name;
          this.categoryName = this.product[0].category[0].name;
          this.categoryLink = this.product[0].category[0].link;
          this.categoryId = this.product[0].category[0].id;
          const homeUrl = '/' + country + '/' + language;
          this.translate.get(['MAINMENUTVS', 'HITACHIHOME'])
          .subscribe(
            translations => {
              this.breadcrumbsService.store([
                {label: translations.HITACHIHOME, url: homeUrl, params: []},
                {label: translations.MAINMENUTVS, url: homeUrl + '/tvs', params: []},
                {label: this.categoryName, url: homeUrl + '/category/' + this.categoryId + '/' + this.categoryLink, params: []},
                {label: this.productName, url: '', params: []}
              ]);
              this.titleService.setTitle(this.productName + ' | ' + environment.siteTitle);
            });
        },
        error => console.log('Error ::' + error)
      );
  }

  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.link = params['link'];
          this.country = params['country'];
          this.language = params['language'];
          this.getProduct(this.id, this.country, this.language);
        }
      );
   }
}
