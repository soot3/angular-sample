export class Product {
  public id: number;
  public name: string;
  public code: string;
  public link: string;
  public keywords: string;
  public priority: number;
  public spec: Spec[];
  public images: Images[];
  public feature: Feature[];
  public file: File[];
  public related: Related[];
  public text: Text[];
  public highlightedText: HighlightedText[];
  public category: Category[];

  constructor(
    id: number,
    name: string,
    code: string,
    link: string,
    keywords: string,
    priority: number,
    spec: Spec[],
    images: Images[],
    feature: Feature[],
    file: File[],
    related: Related[],
    text: Text[],
    highlightedText: HighlightedText[],
    category: Category[]
  ) {
    this.id = id;
    this.name = name;
    this.code = code;
    this.link = link;
    this.keywords = keywords;
    this.priority = priority;
    this.spec = spec;
    this.images = images;
    this.feature = feature;
    this.file = file;
    this.related = related;
    this.text = text;
    this.highlightedText = highlightedText;
    this.category = category;
  }
}

export class Spec {
  public id: number;
  public name: string;
  public children: Children[];

  constructor(id: number, name: string, children: Children[]) {
    this.id = id;
    this.name = name;
    this.children = children;
  }
}

export class Children {
  public name: string;
  public value: string;

  constructor(name: string, value: string) {
    this.name = name;
    this.value = value;
  }
}

export class Images {
  public type: string;
  public name: string;
  public path: string;

  constructor(type: string, name: string, path: string) {
    this.type = type;
    this.name = name;
    this.path = path;
  }
}

export class Feature {
  public name: string;
  public shortDescription: string;
  public longDescription: string;
  public typeName: string;
  public typeCode: string;
  public image: FeatureImage[];

  constructor(name: string, shortDescription: string, longDescription: string, typeName: string, typeCode: string, image: FeatureImage[]) {
    this.name = name;
    this.shortDescription = shortDescription;
    this.longDescription = longDescription;
    this.typeName = typeName;
    this.typeCode = typeCode;
    this.image = image;
  }
}

export class FeatureImage {
  public path: string;
  public typeCode: string;
  public typeName: string;

  constructor(path: string, typeCode: string, typeName: string) {
    this.path = path;
    this.typeCode = typeCode;
    this.typeName = typeName;
  }
}

export class File {
  public name: string;
  public path: string;
  public typeCode: string;
  public typeName: string;

  constructor(name: string, path: string, typeCode: string, typeName: string) {
    this.name = name;
    this.path = path;
    this.typeCode = typeCode;
    this.typeName = typeName;
  }
}

export class Related {
  public name: string;
  public code: string;
  public link: string;
  public file: string;
  public keywords: string;

  constructor(name: string, code: string, link: string, file: string, keywords: string) {
    this.name = name;
    this.code = code;
    this.link = link;
    this.file = file;
    this.keywords = keywords;
  }
}

export class Text {
  public title: string;
  public shortDescription: string;
  public longDescription: string;
  public link: string;
  public keywords: string;

  constructor(title: string, shortDescription: string, longDescription: string, link: string, keywords: string) {
    this.title = title;
    this.shortDescription = shortDescription;
    this.longDescription = longDescription;
    this.link = link;
    this.keywords = keywords;
  }
}

export class HighlightedText {
  public text: string;

  constructor(text: string) {
    this.text = text;
  }
}

export class Category {
  public name: string;
  public link: string;
  public keywords: string;
  public parentId: number;

  constructor(name: string, link: string, keywords: string, parentId: number) {
    this.name = name;
    this.link = link;
    this.keywords = keywords;
    this.parentId = parentId;
  }
}
