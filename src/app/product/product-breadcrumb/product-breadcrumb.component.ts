import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-product-breadcrumb',
  templateUrl: './product-breadcrumb.component.html',
  styleUrls: ['./product-breadcrumb.component.scss']
})
export class ProductBreadcrumbComponent implements OnInit {

  id: number;
  link: string;
  country: string;
  language: string;
  product: any;

  constructor(private productService: ProductService, private route: ActivatedRoute) { }

  getProduct(id: number, country: string, language: string): void {
    this.productService.getProduct(id, country, language)
      .subscribe(
        resultArray => this.product = resultArray,
        error => console.log('Error ::' + error)
    )
  }

  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.link = params['link'];
          this.country = params['country'];
          this.language = params['language'];
          this.getProduct(this.id, this.country, this.language);
      }
    )
  }
}
