import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../product.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-specs',
  templateUrl: './specs.component.html',
  styleUrls: ['./specs.component.scss']
})
export class SpecsComponent implements OnInit {

  link: string;
  id: number;
  country: string;
  language: string;

  specs: any;

  constructor(private specsService: ProductService, private route: ActivatedRoute) { }

  getSpecs(id: number, country: string, language: string): void {
    this.specsService.getSpecs(id, country, language)
      .subscribe(
        resultArray => {
          this.specs = resultArray;
        },
        error => console.log('Error ::' + error)
      );
  }

  ngOnInit(): void {
    this.route.parent.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.link = params['link'];
          this.country = params['country'];
          this.language = params['language'];
          this.getSpecs(this.id, this.country, this.language);
        }
      );
  }

}
