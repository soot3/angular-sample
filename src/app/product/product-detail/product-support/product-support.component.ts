import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../product.service';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-product-support',
  templateUrl: './product-support.component.html',
  styleUrls: ['./product-support.component.scss']
})

export class ProductSupportComponent implements OnInit {

  link: string;
  id: number;
  country: string;
  language: string;
  files: any;

  constructor(private filesService: ProductService, private route: ActivatedRoute) { }

  getFiles(id: number, country: string, language: string): void {
    this.filesService.getFiles(id, country, language)
      .subscribe(
        resultArray => {
          this.files = resultArray;
        },
        error => console.log('Error ::' + error)
    );
  }

  ngOnInit(): void {
    this.route.parent.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.link = params['link'];
          this.country = params['country'];
          this.language = params['language'];
          this.getFiles(this.id, this.country, this.language);
      }
    );
  }
}
