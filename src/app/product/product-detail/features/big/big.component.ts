import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../product.service';
import { ActivatedRoute, Params } from '@angular/router';
import { SafeStyle } from '@angular/platform-browser';

@Component({
  selector: 'app-big',
  templateUrl: './big.component.html',
  styleUrls: ['./big.component.scss']
})
export class BigComponent implements OnInit {

  link: string;
  id: number;
  country: string;
  language: string;
  features: any;
  image: SafeStyle;
  index: number;

  constructor(private featureService: ProductService, private route: ActivatedRoute) { }

  getFeatures(id: number, country: string, language: string): void {
    this.featureService.getFeatures(id, country, language)
      .subscribe(
        resultArray => {
            this.features = resultArray.filter(
              feature => feature.typeCode === 'bigFeatures');
            this.image = resultArray.map(
              (image) => {
                  return image.image[0].path;
              }
            );
        },
        error => console.log('Error ::' + error)
    );
  }

  ngOnInit(): void {
    this.route.parent.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.link = params['link'];
          this.country = params['country'];
          this.language = params['language'];
          this.getFeatures(this.id, this.country, this.language);
      }
    );
  }
}
