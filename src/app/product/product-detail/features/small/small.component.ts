import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../product.service';
import { ActivatedRoute, Params } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-small',
  templateUrl: './small.component.html',
  styleUrls: ['./small.component.scss']
})
export class SmallComponent implements OnInit {

  link: string;
  id: number;
  country: string;
  language: string;
  features: any;

  selectedIndex: number;

  singleSmallFeature = {
    'slidesToShow': 1,
    'slidesToScroll': 1,
    'arrows': false,
    'fade': true,
    'asNavFor': '.small-features'
  };

  smallFeatures = {
    'slidesToShow': 4,
    'slidesToScroll': 1,
    'asNavFor': '.single-small-feature',
    'dots': false,
    'focusOnSelect': true,
    'arrows': true,
  };

  select(index: number) {
      this.selectedIndex = index;
  }

  constructor(private featureService: ProductService, private route: ActivatedRoute) { }

  getFeatures(id: number, country: string, language: string): void {
    this.featureService.getFeatures(id, country, language)
      .subscribe(
        resultArray => {
            this.features = resultArray.filter(
              feature => feature.typeCode === 'smallFeatures');
        },
        error => console.log('Error ::' + error)
    );
  }

  changeTab(dataTab: string) {
    const tab_id = dataTab;

    $('#thumb' + tab_id).siblings().removeClass('is-active');
    $('.single-small-feature').removeClass('is-active');

    $('#thumb' + tab_id).addClass('is-active');
    $('#tab' + tab_id).addClass('is-active');
  }

  ngOnInit(): void {
    this.route.parent.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.link = params['link'];
          this.country = params['country'];
          this.language = params['language'];
          this.getFeatures(this.id, this.country, this.language);
      }
    );
  }
}
