import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { Product } from './product.model';

@Injectable()
export class ProductService {

  constructor(private http: HttpClient) {}

  getProduct(id: number, country: string, language: string) {
    return this.http
    .get(environment.rootUrl + country + '/' + language + '/product/' + id)
    .map(
      (product) => {
        return product;
    });
  }

  getHighlightedText(id: number, country: string, language: string) {
    return this.http
    .get<Product[]>(environment.rootUrl + country + '/' + language + '/product/' + id)
    .map(
      (highlightedTexts) => {
        return highlightedTexts[0].highlightedText;
    });
  }

  getFeatures(id: number, country: string, language: string) {
    return this.http
    .get<Product[]>(environment.rootUrl + country + '/' + language + '/product/' + id)
    .map(
      (features) => {
        return features[0].feature;
    });
  }

  getSpecs(id: number, country: string, language: string) {
    return this.http
    .get<Product[]>(environment.rootUrl + country + '/' + language + '/product/' + id)
    .map(
      (specs) => {
        return specs[0].spec;
    });
  }

  getFiles(id: number, country: string, language: string) {
    return this.http
    .get<Product[]>(environment.rootUrl + country + '/' + language + '/product/' + id)
    .map(
      (files) => {
        return files[0].file;
    });
  }

  getImages(id: number, country: string, language: string) {
    return this.http
      .get<Product[]>(environment.rootUrl + country + '/' + language + '/product/' + id)
      .map(
        (images) => {
          return images[0].images;
        }
      );
  }


}
