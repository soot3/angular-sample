import { Component, OnInit, Inject } from '@angular/core';
import { LocationService } from './shared/location.service';
import { TranslateService } from '@ngx-translate/core';
import { Title, DOCUMENT } from '@angular/platform-browser';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';

  subscription: Subscription;
  locations: any;
  ipAddress: any;
  country: any;
  ipForLocation: any;
  language: string;
  langCode: any;
  countryCode: any;
  countryLanguage: string;

  constructor(
    private locationService: LocationService,
    public translate: TranslateService,
    public titleService: Title,
    private router: Router,
    @Inject(DOCUMENT) private _document: any
  ) {
    // translate.addLangs(['en', 'tr']);
    // translate.setDefaultLang('en');

    // translate.onLangChange.map(x => x.lang)
    //   .subscribe(lang => this._document.documentElement.lang = lang);
  }

  getLocations(): void {
    this.locationService.getLocations()
      .subscribe(
        resultArray => {
          this.locations = resultArray;
        },
        error => console.log('Error ::' + error)
      );
  }

  getIpAddress() {
    this.locationService.getIpAddress();
  }

  // getIpAddress(): void {
  //   this.locationService.getIpAddress()
  //     .subscribe(
  //       ipArray => {
  //         this.ipAddress = ipArray;
  //         this.country = this.locationService.getCountryFromIp(this.ipAddress)
  //           .subscribe(
  //             ipAddressArray => {
  //               this.country = ipAddressArray.toLowerCase();
  //               this.locationService.getLocation()
  //                 .subscribe(
  //                   resultArray => {
  //                     this.locations = resultArray;
  //                     this.countryCode = this.locations.filter(
  //                       location => location.code === this.country
  //                     );
  //                     this.langCode = this.countryCode.map(
  //                       (language) => {
  //                         return language.lang[0].code;
  //                       }
  //                     );

  //                     // if (!Array.isArray(this.countryCode) || !this.countryCode.length) {
  //                       this.countryLanguage = 'eu/en';
  //                       this.router.navigate(['/' + this.countryLanguage]);
  //                     // } else {
  //                     //   this.countryLanguage = this.country + '/' + this.langCode;
  //                     //   this.router.navigate(['/' + this.countryLanguage]);
  //                     // }


  //                     // this.countryLanguage = this.country + '/' + this.langCode;
  //                     // this.countryLanguage = this.country + '/' + this.langCode;
  //                     // this.countryLanguage = 'eu/en';
  //                     // this.router.navigate(['/eu/en/category/1/OLED']);
  //                   },
  //                   error => console.log('Error ::' + error)
  //                 );
  //             },
  //             error => console.log('Error ::' + error)
  //           );
  //       },
  //       error => console.log('Error ::' + error)
  //     );
  // }

  ngOnInit() {
    setTimeout(() => {
      $(document).foundation();
    }, 1500);

    const pathname = window.location.pathname;
    const pathnameSplit = pathname.split('/');
    this.country = pathnameSplit[1];
    this.language = pathnameSplit[2];

    this.translate.onLangChange.map(x => x.lang)
    .subscribe(lang => this._document.documentElement.lang = lang);

    // if (!this.country) {
      // this.router.navigate(['/' + this.country + '/' + this.language]);
      this.getIpAddress();
      // this.translate.use(this.language);
    // }
  }
}
