
import { Pipe, PipeTransform } from '@angular/core';
 
@Pipe({
  name: 'uppercase'
})
export class UppercasePipes implements PipeTransform {
 
  transform(text: any) {
    return text.toUpperCase()
  }
}
