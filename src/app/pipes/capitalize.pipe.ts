
import { Pipe, PipeTransform } from '@angular/core';
 
@Pipe({
  name: 'capitalize'
})
export class CapitalizePipes implements PipeTransform {
 
  transform(text: any) {
    return text.toLowerCase()
        .split(' ')
        .map((value: string) => FirstVal(value))
        .join(' ');
   }
}

export function FirstVal (value: string) {
  return value.slice(0, 1).toUpperCase() + value.slice(1);
}

 