
import { Pipe, PipeTransform } from '@angular/core';
 
@Pipe({
  name: 'lowercase'
})
export class LowercasePipes implements PipeTransform {
 
  transform(text: any) {
    return text.toLowerCase()
  }
}
