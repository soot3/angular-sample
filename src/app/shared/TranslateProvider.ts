import {Injectable} from '@angular/core';
import { LocationService } from './location.service';

@Injectable()
export class TranslateProvider {

  private _currentLang: string;
  // If you want to use dictionary from local resources
  private _dictionary = {
  };

  // inject our translations
  constructor( private _db: LocationService) { }

  public get currentLang() {
     if ( this._currentLang !== null) {
       return this._currentLang;
     } else {
       return 'en';
      }
  }

  public use(lang: string): void {
    this._currentLang = lang;
  } // set current language

  // private perform translation
  private translate(key: string): string {

   // if u use local files
    if (this._dictionary[this.currentLang] && this._dictionary[this.currentLang][key]) {
      return this._dictionary[this.currentLang][key];
    } else {
      return key;
    }

   // if u do not want local files then get a json file from database and add to dictionary
  }

  public instant(key: string) { return this.translate(key); }
}
