export class Location {
  public id: number;
  public name: string;
  public origName: string;
  public code: string;
  public priority: number;
  public lang: Lang[];

  constructor(
    id: number,
    name: string,
    origName: string,
    code: string,
    priority: number,
    lang: Lang[]
  ) {
    this.id = id;
    this.name = name;
    this.origName = origName;
    this.code = code;
    this.priority = priority;
    this.lang = lang;
  }
}

export class Lang {
  public id: number;
  public name: string;
  public origName: string;
  public code: string;

  constructor(
    id: number,
    name: string,
    origName: string,
    code: string
  ) {
    this.id = id;
    this.name = name;
    this.origName = origName;
    this.code = code;
  }
}
