import { Pipe, PipeTransform } from '@angular/core';

import {TranslateProvider} from '../TranslateProvider';

@Pipe({
  name: 'translation',
})
export class TranslationPipe implements PipeTransform {

  constructor(private _translateService: TranslateProvider) { }

  transform(value: string, ...args) {
    return this._translateService.instant(value);
  }
}
