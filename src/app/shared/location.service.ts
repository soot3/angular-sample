import { Injectable, Inject } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

import { Location } from '../shared/location.model';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { DOCUMENT } from '@angular/common';
import { SocialMediaService } from './social-media.service';
import { CategoryService } from '../category/category.service';
import { Category } from '../category/category.model';

@Injectable()
export class LocationService {

  //   private locations: Observable<any[]>;

  ipAddress: string;
  country: any;
  locations: any;
  countryCode: any;
  langCode: any;
  countryLanguage: any;
  mainLang: any;
  languageCode: any;

  categoriesArray: Category[];

  public socialMediaArray: any;
  public socialMediaElements: any;

  constructor(
    private http: HttpClient,
    private router: Router,
    private translate: TranslateService,
    private socialMediaService: SocialMediaService,
    private categoryService: CategoryService,
    @Inject(DOCUMENT) private _document: any
  ) { }

  getLocations() {
    return this.http
      .get(environment.rootUrl + 'tr/tr/locations')
      .map(
        (locations) => {
          return locations;
        });
  }

  getLocation() {
    return this.http
      .get(environment.rootUrl + 'tr/tr/locations')
      .map(
        (locations) => {
          return locations;
        });
  }

  getIpAddress() {
    return this.http
      .get('http://ipinfo.io/')
      .map(
        (ipAddress) => {
          return ipAddress['ip'];
        })
      .subscribe(
        ipArray => {
          this.ipAddress = ipArray;
          this.country = this.getCountryFromIp(this.ipAddress)
            .subscribe(
              ipAddressArray => {
                this.country = ipAddressArray.toLowerCase();
                this.getLocation()
                  .subscribe(
                    resultArray => {
                      this.locations = resultArray;
                      this.countryCode = this.locations.filter(
                        location => location.code === this.country
                      );
                      this.languageCode = this.countryCode.map(
                        (language) => {
                          language.lang.map(
                            (main) => {
                              if (main.main === 1) {
                                this.langCode = main.code;
                                console.log(this.langCode);
                              }
                            }
                          );
                        }
                      );

                      if (!Array.isArray(this.countryCode) || !this.countryCode.length) {
                        this.countryLanguage = 'eu/en';
                        this.router.navigate(['/' + this.countryLanguage]);
                        this.translate.use('en');
                        this.socialMediaService.getSocialMedia('eu', 'en')
                          .subscribe(
                            (socialMediaResultArray) => {
                              this.socialMediaArray = socialMediaResultArray;
                              environment.socialMediaElements = this.socialMediaArray;
                            },
                            error => console.log('Error ::' + error)
                          );
                        this.categoryService.getCategories('eu', 'en')
                          .subscribe(
                            categoryResultArray => this.categoriesArray = categoryResultArray,
                            error => console.log('Error ::' + error)
                          );
                      } else {
                        this.countryLanguage = this.country + '/' + this.langCode;
                        this.router.navigate(['/' + this.countryLanguage]);
                        this.translate.use(this.langCode);
                        console.log(this.langCode);
                        this.translate.onLangChange.map(x => x.lang)
                          .subscribe(lang => this._document.documentElement.lang = lang);
                          this.socialMediaService.getSocialMedia(this.country, this.langCode)
                          .subscribe(
                            (socialMediaResultArray) => {
                              this.socialMediaArray = socialMediaResultArray;
                              environment.socialMediaElements = this.socialMediaArray;
                            },
                            error => console.log('Error ::' + error)
                          );
                        this.categoryService.getCategories(this.country, this.langCode)
                          .subscribe(
                            categoryResultArray => this.categoriesArray = categoryResultArray,
                            error => console.log('Error ::' + error)
                          );
                      }
                    },
                    error => console.log('Error ::' + error)
                  );
              },
              error => console.log('Error ::' + error)
            );
        },
        error => console.log('Error ::' + error)
      );
  }

  getSocialMediaArray() {
    console.log(environment.socialMediaElements);
    return environment.socialMediaElements;
  }

  getCountryFromIp(ip: string) {
    return this.http
      .get('http://ipinfo.io/' + ip)
      .map(
        (country) => {
          return country['country'];
        });
  }


  // getIpAddress()
  //     .subscribe(
  //       ipArray => {
  //         this.ipAddress = ipArray;
  //         this.country = this.locationService.getCountryFromIp(this.ipAddress)
  //           .subscribe(
  //             ipAddressArray => {
  //               this.country = ipAddressArray.toLowerCase();
  //               this.locationService.getLocation()
  //                 .subscribe(
  //                   resultArray => {
  //                     this.locations = resultArray;
  //                     this.countryCode = this.locations.filter(
  //                       location => location.code === this.country
  //                     );
  //                     this.langCode = this.countryCode.map(
  //                       (language) => {
  //                         return language.lang[0].code;
  //                       }
  //                     );

  //                     // if (!Array.isArray(this.countryCode) || !this.countryCode.length) {
  //                       this.countryLanguage = 'eu/en';
  //                       this.router.navigate(['/' + this.countryLanguage]);
  //                     // } else {
  //                     //   this.countryLanguage = this.country + '/' + this.langCode;
  //                     //   this.router.navigate(['/' + this.countryLanguage]);
  //                     // }


  //                     // this.countryLanguage = this.country + '/' + this.langCode;
  //                     // this.countryLanguage = this.country + '/' + this.langCode;
  //                     // this.countryLanguage = 'eu/en';
  //                     // this.router.navigate(['/eu/en/category/1/OLED']);
  //                   },
  //                   error => console.log('Error ::' + error)
  //                 );
  //             },
  //             error => console.log('Error ::' + error)
  //           );
  //       },
  //       error => console.log('Error ::' + error)
  //     );

}
