import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/operators/map';
import { environment } from '../../environments/environment.prod';

@Injectable()
export class SocialMediaService {

    constructor(private http: HttpClient) {}

    getSocialMedia(country: string, language: string) {
        return this.http
          .get(environment.rootUrl + country + '/' + language + '/socialmedia')
          .map(
          (socialmedia) => {
              return socialmedia;
          });
    }

}
