import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PageService {
    pageTitle: string;

    lastSegment: any;

    subject: Subject<any> = new Subject<any>();

    constructor(private http: HttpClient) {}

    sendTitle(pageTitle: string) {
      this.subject.next({ text: pageTitle });
    }

    clearTitle() {
      this.subject.next();
    }

    getTitle(): Observable<any> {
      return this.subject.asObservable();
    }

    getCustomPages(country: string, language: string) {
        return this.http
        .get(environment.rootUrl + country + '/' + language + '/custompage/', {
            responseType: 'text'
        })
        .map(
        (custom) => {
            return custom;
        });
    }
}



