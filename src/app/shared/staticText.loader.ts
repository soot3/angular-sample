import { Injectable, Injector } from '@angular/core';
import { TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment.prod';

@Injectable()
export class StaticTextLoader implements TranslateLoader {

  country: string;
  language: string;

  constructor(private inej: Injector) {}

  getTranslation(lang: string): Observable<any> {

    const pathname = window.location.pathname;
    const pathnameSplit = pathname.split('/');
    this.country = pathnameSplit[1];
    this.language = pathnameSplit[2];

    const http = this.inej.get(HttpClient);

    if (this.country === undefined || !this.country) {
      return http.get(environment.rootUrl + environment.defaultCountry + '/' + lang + '/statictext');
    } else {
      return http.get(environment.rootUrl + this.country + '/' + lang + '/statictext');
    }

  }
}
