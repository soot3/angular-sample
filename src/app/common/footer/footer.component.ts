import { Component, OnInit } from '@angular/core';
import { SocialMediaService } from '../../shared/social-media.service';
import { ActivatedRoute, Params } from '@angular/router';
import { LocationService } from 'src/app/shared/location.service';
import { SimpleGlobal } from 'ng2-simple-global';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  socialMedia: any;
  country: string;
  language: string;

  constructor(
    // private socialMediaService: SocialMediaService,
    // private route: ActivatedRoute
    private locationService: LocationService
  ) {}

  // getSocialMedia(country: string, language: string): void {
  //   this.socialMediaService.getSocialMedia(country, language)
  //     .subscribe(
  //       resultArray => {
  //         this.socialMedia = resultArray;
  //       },
  //       error => console.log('Error ::' + error)
  //     );
  // }

  ngOnInit() {
    this.locationService.getSocialMediaArray();
    // this.socialMedia = this.sg['socialMediaArray'];
    // console.log(this.socialMedia);
    // this.route.params
    // .subscribe(
    //   (params: Params) => {
    //     this.country = params['country'];
    //     this.language = params['language'];
    //     this.getSocialMedia(this.country, this.language);
    //   }
    // );
    // setTimeout(() => {
    //   const pathname = window.location.pathname;
    //   const pathnameSplit = pathname.split('/');
    //   this.country = pathnameSplit[1];
    //   this.language = pathnameSplit[2];
    //   this.getSocialMedia(this.country, this.language);
    // }, 1500);
  }

}
