import { Component, OnInit, OnDestroy } from '@angular/core';
import { CategoryService } from '../../../category/category.service';
import { Category } from '../../../category/category.model';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.scss']
})
export class MainNavigationComponent implements OnInit, OnDestroy {

  country: string;
  language: string;

  subscription: Subscription;

  categoriesArray: Category[];

  public enableFutureLink = false;

  constructor(
    private categoryService: CategoryService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {

    this.route.params
      .subscribe(
        (params: Params) => {
          this.country = params['country'];
          this.language = params['language'];
          this.subscription = this.categoryService.getCategories(this.country, this.language)
            .subscribe(
              resultArray => this.categoriesArray = resultArray,
              error => console.log('Error ::' + error)
            );
        }
      );

    // setTimeout(() => {
    //   const pathname = window.location.pathname;
    //   const pathnameSplit = pathname.split('/');
    //   this.country = pathnameSplit[1];
    //   this.language = pathnameSplit[2];
    //   this.subscription = this.categoryService.getCategories(this.country, this.language)
    //   .subscribe(
    //     resultArray => this.categoriesArray = resultArray,
    //     error => console.log('Error ::' + error)
    //   );
    // }, 1500);

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
