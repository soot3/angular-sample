import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  country: string;
  language: string;

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      const pathname = window.location.pathname;
      const pathnameSplit = pathname.split('/');
      this.country = pathnameSplit[1];
      this.language = pathnameSplit[2];
    }, 1500);
  }

}
