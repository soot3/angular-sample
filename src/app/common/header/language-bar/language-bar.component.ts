import { Component, OnInit, OnDestroy } from '@angular/core';
import { LocationService } from '../../../shared/location.service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-language-bar',
  templateUrl: './language-bar.component.html',
  styleUrls: ['./language-bar.component.scss']
})
export class LanguageBarComponent implements OnInit, OnDestroy {

  country: string;
  language: string;
  public subscription: Subscription;

  countryParam: string;
  countryName: string;
  languageName: string;
  languageCode: string;
  countryCode: string;
  countries: any;

  constructor(private locationService: LocationService, public translateService: TranslateService) { }

  getLocation(countryParam: string): void {
    this.subscription = this.locationService.getLocation()
      .subscribe(
        resultArray => {
          this.countries = resultArray;
          for (const countrySingle of this.countries) {
            if ( countrySingle.code === countryParam ) {
              this.countryName = countrySingle.origName;
              this.countryCode = countrySingle.code;
            }
            for (const language of countrySingle.lang) {
              if ( language.code === this.translateService.currentLang ) {
                this.languageName = language.origName;
                this.languageCode = this.translateService.currentLang;
              }
            }
          }
        },
        error => console.log('Error ::' + error)
      );
  }

  ngOnInit() {
    setTimeout(() => {
      const pathname = window.location.pathname;
      const pathnameSplit = pathname.split('/');
      this.country = pathnameSplit[1];
      this.language = pathnameSplit[2];
      this.getLocation(this.country);
    }, 500);

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
