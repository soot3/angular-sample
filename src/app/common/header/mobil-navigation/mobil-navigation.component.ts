import { Component, OnInit } from '@angular/core';
import { LocationService } from '../../../shared/location.service';
import { TranslateService } from '@ngx-translate/core';
import { CategoryService } from '../../../category/category.service';
import { Category } from '../../../product/product.model';

@Component({
  selector: 'app-mobil-navigation',
  templateUrl: './mobil-navigation.component.html',
  styleUrls: ['./mobil-navigation.component.scss']
})
export class MobilNavigationComponent implements OnInit {

  country: string;
  language: string;
  countryParam: string;
  countryName: string;
  languageName: string;
  languageCode: string;
  countryCode: string;
  countries: any;

  categoriesArray: Category[];

  isOpened = false;

  constructor(
    private locationService: LocationService,
    private categoryService: CategoryService,
    public translateService: TranslateService
  ) { }

  getCategories(country: string, language: string): void {
    this.categoryService.getCategories(country, language)
      .subscribe(
        resultArray => this.categoriesArray = resultArray,
        error => console.log('Error ::' + error)
      );
  }

  getLocation(countryParam: string): void {
    this.locationService.getLocation()
      .subscribe(
        resultArray => {
          this.countries = resultArray;
          for (const countrySingle of this.countries) {
            if (countrySingle.code === countryParam) {
              this.countryName = countrySingle.origName;
              this.countryCode = countrySingle.code;
            }
            for (const language of countrySingle.lang) {
              if (language.code === this.translateService.currentLang) {
                this.languageName = language.origName;
                this.languageCode = this.translateService.currentLang;
              }
            }
          }
        },
        error => console.log('Error ::' + error)
      );
  }

  onMenuButton(event) {
    this.isOpened = !this.isOpened;
  }

  ngOnInit() {
    setTimeout(() => {
      const pathname = window.location.pathname;
      const pathnameSplit = pathname.split('/');
      this.country = pathnameSplit[1];
      this.language = pathnameSplit[2];
      this.getCategories(this.country, this.language);
      this.getLocation(this.country);
    }, 500);

  }



}
