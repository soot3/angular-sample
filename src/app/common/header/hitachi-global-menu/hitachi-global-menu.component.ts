import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-hitachi-global-menu',
  templateUrl: './hitachi-global-menu.component.html',
  styleUrls: ['./hitachi-global-menu.component.scss']
})
export class HitachiGlobalMenuComponent implements OnInit {

  continentList = [
    {
      name: 'Americas',
      id: 'americas',
      countries: [
        {
          name: 'Brazil',
          url: 'http://www.hitachi.com.br/',
          lang: 'Português',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'pt',
          langCode2: '',
          langCode3: '',
          col: '1'
        },
        {
          name: 'Canada',
          url: 'http://www.hitachi.ca/',
          lang: 'English',
          url2: 'http://www.hitachi.ca/fre/',
          lang2: 'Français',
          url3: '',
          lang3: '',
          langCode: '',
          langCode2: 'fr',
          langCode3: '',
          col: '1'
        },
        {
          name: 'Mexico',
          url: 'http://www.hitachi.com.mx/',
          lang: 'Español',
          url2: 'http://www.hitachi.com.mx/',
          lang2: 'English',
          url3: '',
          lang3: '',
          langCode: 'es',
          langCode2: '',
          langCode3: '',
          col: '2'
        },
        {
          name: 'South America',
          url: 'http://www.hitachi.com.ar/',
          lang: 'Español',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'es',
          langCode2: '',
          langCode3: '',
          col: '2'
        },
        {
          name: 'U.S.A.',
          url: 'http://www.hitachi.us/index.html',
          lang: 'English',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: '',
          langCode2: '',
          langCode3: '',
          col: '3'
        }
      ]
    },
    {
      name: 'Asia',
      id: 'asia',
      countries: [
        {
          name: 'China',
          url: 'http://www.hitachi.com.cn/',
          lang: '简体中文',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'zh',
          langCode2: '',
          langCode3: '',
          col: '1'
        },
        {
          name: 'Hong Kong',
          url: 'http://www.hitachi.com.hk/',
          lang: '繁體中文',
          url2: 'http://www.hitachi.com.hk/eng/',
          lang2: 'English',
          url3: '',
          lang3: '',
          langCode: 'zh-TW',
          langCode2: '',
          langCode3: '',
          col: '1'
        },
        {
          name: 'India',
          url: 'http://www.hitachi.co.in/',
          lang: 'English',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: '',
          langCode2: '',
          langCode3: '',
          col: '1'
        },
        {
          name: 'Indonesia',
          url: 'http://www.hitachi.co.id/',
          lang: 'Bahasa Indonesia',
          url2: 'http://www.hitachi.co.id/eng/',
          lang2: 'English',
          url3: '',
          lang3: '',
          langCode: '',
          langCode2: '',
          langCode3: '',
          col: '1'
        },
        {
          name: 'Japan',
          url: 'http://www.hitachi.co.jp/',
          lang: '日本語',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'ja',
          langCode2: '',
          langCode3: '',
          col: '2'
        },
        {
          name: 'Korea',
          url: 'http://www.hitachi.co.kr/',
          lang: '한국어',
          url2: 'http://www.hitachi.co.kr/jpn/',
          lang2: '한국어',
          url3: '',
          lang3: '',
          langCode: 'ko',
          langCode2: 'ja',
          langCode3: '',
          col: '2'
        },
        {
          name: 'Malaysia',
          url: 'http://www.hitachi.com.my/',
          lang: 'English',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: '',
          langCode2: '',
          langCode3: '',
          col: '2'
        },
        {
          name: 'Philippines',
          url: 'http://www.hitachi.com.ph/',
          lang: 'English',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: '',
          langCode2: '',
          langCode3: '',
          col: '2'
        },
        {
          name: 'Singapore',
          url: 'https://www.hitachi.com.sg/',
          lang: 'English',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: '',
          langCode2: '',
          langCode3: '',
          col: '3'
        },
        {
          name: 'Taiwan',
          url: 'http://www.hitachi.com.tw/',
          lang: '繁體中文',
          url2: 'http://www.hitachi.com.tw/eng/',
          lang2: 'English',
          url3: '',
          lang3: '',
          langCode: 'zh-TW',
          langCode2: '',
          langCode3: '',
          col: '3'
        },
        {
          name: 'Thailand',
          url: 'http://www.hitachi.co.th/',
          lang: 'ไทย',
          url2: 'http://www.hitachi.co.th/eng/',
          lang2: 'Englush',
          url3: '',
          lang3: '',
          langCode: 'th',
          langCode2: '',
          langCode3: '',
          col: '3'
        },
        {
          name: 'Vietnam',
          url: 'http://www.hitachi.com.vn/',
          lang: 'Tiếng Việt',
          url2: 'http://www.hitachi.com.vn/eng/',
          lang2: 'English',
          url3: '',
          lang3: '',
          langCode: 'vi',
          langCode2: '',
          langCode3: '',
          col: '3'
        },
      ]
    },
    {
      name: 'Europe',
      id: 'europe',
      countries: [
        {
          name: 'Europe Gateway',
          url: 'http://www.hitachi.eu/en/',
          lang: 'English',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: '',
          langCode2: '',
          langCode3: '',
          col: '1'
        },
        {
          name: 'Austria',
          url: 'http://www.hitachi.eu/de-at/',
          lang: 'Deutsch',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'de',
          langCode2: '',
          langCode3: '',
          col: '1'
        },
        {
          name: 'Belgium',
          url: 'http://www.hitachi.eu/fr-be/',
          lang: 'Français',
          url2: 'http://www.hitachi.eu/nl-be/',
          lang2: 'Nederlands',
          url3: '',
          lang3: '',
          langCode: 'fr',
          langCode2: 'nl',
          langCode3: '',
          col: '1'
        },
        {
          name: 'Czech',
          url: 'http://www.hitachi.eu/cs-cz/',
          lang: 'Čeština',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'cs',
          langCode2: '',
          langCode3: '',
          col: '1'
        },
        {
          name: 'Denmark',
          url: 'http://www.hitachi.eu/da-dk/',
          lang: 'Dansk',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'da',
          langCode2: '',
          langCode3: '',
          col: '1'
        },
        {
          name: 'Finland',
          url: 'http://www.hitachi.eu/fi-fi/',
          lang: 'Suomi',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'fi',
          langCode2: '',
          langCode3: '',
          col: '1'
        },
        {
          name: 'France',
          url: 'http://www.hitachi.eu/fr-fr/',
          lang: 'Français',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'fr',
          langCode2: '',
          langCode3: '',
          col: '1'
        },
        {
          name: 'Germany',
          url: 'http://www.hitachi.eu/de-de/',
          lang: 'Deutsch',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'de',
          langCode2: '',
          langCode3: '',
          col: '1'
        },
        {
          name: 'Greece',
          url: 'http://www.hitachi.eu/el-gr/',
          lang: 'Ελληνικά',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'el',
          langCode2: '',
          langCode3: '',
          col: '1'
        },
        {
          name: 'Ireland',
          url: 'http://www.hitachi.eu/en-ie/',
          lang: 'English',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: '',
          langCode2: '',
          langCode3: '',
          col: '2'
        },
        {
          name: 'Italy',
          url: 'http://www.hitachi.eu/it-it/',
          lang: 'Italiano',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'it',
          langCode2: '',
          langCode3: '',
          col: '2'
        },
        {
          name: 'Lithuania',
          url: 'http://www.hitachi.eu/lt-lt/',
          lang: 'Lietuvių',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'lt',
          langCode2: '',
          langCode3: '',
          col: '2'
        },
        {
          name: 'Netherlands',
          url: 'http://www.hitachi.eu/nl-nl/',
          lang: 'Nederlands',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'nl',
          langCode2: '',
          langCode3: '',
          col: '2'
        },
        {
          name: 'Norway',
          url: 'http://www.hitachi.eu/no-no/',
          lang: 'Norsk',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: '',
          langCode2: '',
          langCode3: '',
          col: '2'
        },
        {
          name: 'Poland',
          url: 'http://www.hitachi.eu/pl-pl/',
          lang: 'Język Polski',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'pl',
          langCode2: '',
          langCode3: '',
          col: '2'
        },
        {
          name: 'Portugal',
          url: 'http://www.hitachi.eu/pt-pt/',
          lang: 'Português',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'pt',
          langCode2: '',
          langCode3: '',
          col: '2'
        },
        {
          name: 'Romania',
          url: 'http://www.hitachi.eu/ro-ro/',
          lang: 'Română',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'ro',
          langCode2: '',
          langCode3: '',
          col: '2'
        },
        {
          name: 'Russia',
          url: 'http://www.hitachi.ru/',
          lang: 'Русский язык',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'ru',
          langCode2: '',
          langCode3: '',
          col: '3'
        },
        {
          name: 'Slovakia',
          url: 'http://www.hitachi.eu/sk-sk/',
          lang: 'Русский язык',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'sk',
          langCode2: '',
          langCode3: '',
          col: '3'
        },
        {
          name: 'Spain',
          url: 'http://www.hitachi.eu/es-es/',
          lang: 'Español',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'es',
          langCode2: '',
          langCode3: '',
          col: '3'
        },
        {
          name: 'Sweden',
          url: 'http://www.hitachi.eu/sv-se/',
          lang: 'Svenska',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'sv',
          langCode2: '',
          langCode3: '',
          col: '3'
        },
        {
          name: 'Switzerland',
          url: 'http://www.hitachi.eu/it-ch/',
          lang: 'Italiano',
          url2: 'http://www.hitachi.eu/de-ch/',
          lang2: 'Deutsch',
          url3: 'http://www.hitachi.eu/fr-ch/',
          lang3: 'Français',
          langCode: 'it',
          langCode2: 'de',
          langCode3: 'fr',
          col: '3'
        },
        {
          name: 'Turkey',
          url: 'http://www.hitachi.eu/',
          lang: 'Türkçe',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: 'tr',
          langCode2: '',
          langCode3: '',
          col: '3'
        },
        {
          name: 'United Kingdom',
          url: 'http://www.hitachi.eu/en-gb/',
          lang: 'English',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: '',
          langCode2: '',
          langCode3: '',
          col: '3'
        }
      ]
    },
    {
      name: 'Middle East and Africa',
      id: 'emea',
      countries: [
        {
          name: 'Middle East and North Africa',
          url: 'http://www.hitachi.ae/',
          lang: 'اللغة العربية',
          url2: 'http://www.hitachi.ae/eng/',
          lang2: 'English',
          url3: '',
          lang3: '',
          langCode: 'ar',
          langCode2: '',
          langCode3: '',
          col: '1'
        },
        {
          name: 'Sub-Saharan Africa',
          url: 'http://www.hitachi.co.za/',
          lang: 'English',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: '',
          langCode2: '',
          langCode3: '',
          col: '2'
        }
      ]
    },
    {
      name: 'Oceania',
      id: 'oceania',
      countries: [
        {
          name: 'Oceania',
          url: 'http://www.hitachi.com.au/',
          lang: 'English',
          url2: '',
          lang2: '',
          url3: '',
          lang3: '',
          langCode: '',
          langCode2: '',
          langCode3: '',
          col: '1'
        }
      ]
    }
  ];

  country: string;
  language: string;

  constructor() { }

  changeContinentTab(event) {
    const tabId = event.target.getAttribute('data-continent');

    $('.header-dropdowns-container ul.tabs li').siblings().removeClass('is-active');
    $('.header-dropdowns-container .tabs-content').removeClass('is-active');
    $('#main-top ul li').removeClass('is-active');

    $('.header-dropdowns-container ul.tabs li[data-continent=' + tabId + ']').addClass('is-active');
    $('.header-dropdowns-container .tabs-content#' + tabId).addClass('is-active');
  }

  changeTopTab(event) {
    const tabId = event.target.getAttribute('id');
    const contentId = tabId.split('-')[0];

    $('#main-top ul li span#' + tabId).parent().siblings().removeClass('is-active');
    $('.header-dropdowns-container .header-dropdown-content').removeClass('is-active');

    $('#main-top ul li span#' + tabId).parent().toggleClass('is-active');


    $('.header-dropdowns-container .header-dropdown-content#' + contentId).addClass('is-active');
  }

  onClose() {
    $('.header-dropdowns-container .header-dropdown-content').removeClass('is-active');
  }


  ngOnInit() {

    setTimeout(() => {
      const pathname = window.location.pathname;
      const pathnameSplit = pathname.split('/');
      this.country = pathnameSplit[1];
      this.language = pathnameSplit[2];
    }, 500);


  }

}
