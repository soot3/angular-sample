import { Component, OnInit, OnDestroy } from '@angular/core';
import { LocationService } from '../../shared/location.service';
import { TranslateService } from '@ngx-translate/core';
import { BreadcrumbsService } from 'ng6-breadcrumbs';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.scss']
})
export class LanguagesComponent implements OnInit, OnDestroy {
  names: any;

  subscription: Subscription;

  locations: any;

  country: string;
  language: string;

  constructor(
    private locationService: LocationService,
    private translate: TranslateService,
    private breadcrumbsService: BreadcrumbsService,
    private titleService: Title,
    private route: ActivatedRoute
  ) {}


  getLocations(): void {
    this.locationService.getLocations()
      .subscribe(
        resultArray => {
          this.locations = resultArray;
        },
        error => console.log('Error ::' + error)
      );
  }


  ngOnInit() {
    this.getLocations();
    this.subscription = this.route.params
      .subscribe(
        (params: Params) => {
          this.country = params['country'];
          this.language = params['language'];

          const homeUrl = '/' + this.country + '/' + this.language;

          this.translate.get(['LANGUAGES', 'HITACHIHOME'])
          .subscribe(
            translations => {
              this.breadcrumbsService.store([
                {label: translations.HITACHIHOME, url: homeUrl, params: []},
                {label: translations.LANGUAGES, url: '', params: []}
              ]);

              this.titleService.setTitle(translations.LANGUAGES + ' - ' + environment.siteTitle);
              // console.log(this.sendTitle('About Us'));
            });
        }
      );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
