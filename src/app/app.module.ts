import { LocationService } from './shared/location.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Modules
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { ResponsiveModule } from 'ngx-responsive';
import { SlickModule } from 'ngx-slick';
import { BreadcrumbsModule } from 'ng6-breadcrumbs';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { StaticTextLoader } from './shared/staticText.loader';
import { AppRoutingModule } from './app-routing.module';
import { SimpleGlobal } from 'ng2-simple-global';

// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SliderComponent } from './home/slider/slider.component';
import { FeaturedProductComponent } from './home/featured-product/featured-product.component';
import { FeaturedSpecsComponent } from './home/featured-specs/featured-specs.component';
import { ProductsBySizeComponent } from './home/products-by-size/products-by-size.component';
import { HeaderComponent } from './common/header/header.component';
import { LanguagesComponent } from './common/languages/languages.component';
import { FooterComponent } from './common/footer/footer.component';
import { ProductComponent } from './product/product.component';
import { FeaturesComponent } from './product/product-detail/features/features.component';
import { SpecsComponent } from './product/product-detail/specs/specs.component';
import { ProductSupportComponent } from './product/product-detail/product-support/product-support.component';
import { CategoryComponent } from './category/category.component';
import { MobilNavigationComponent } from './common/header/mobil-navigation/mobil-navigation.component';
import { HitachiGlobalMenuComponent } from './common/header/hitachi-global-menu/hitachi-global-menu.component';
import { MainNavigationComponent } from './common/header/main-navigation/main-navigation.component';
import { LanguageBarComponent } from './common/header/language-bar/language-bar.component';
import { ImagesComponent } from './product/images/images.component';
import { HighlightedTextComponent } from './product/highlighted-text/highlighted-text.component';
import { BigComponent } from './product/product-detail/features/big/big.component';
import { SmallComponent } from './product/product-detail/features/small/small.component';

// Services
import { SocialMediaService } from './shared/social-media.service';
import { CategoryService } from './category/category.service';
import { ProductService } from './product/product.service';
import { FeaturedProductService } from './home/featured-product/featuredProduct.service';
import { FeaturedSpecsService } from './home/featured-specs/featuredSpecs.service';
import { SlidesService } from './home/slider/slides.service';

// Pipes
import { CapitalizePipes } from './shared/pipes/capitalize.pipe';
import { FilterPipe } from './pipes/filter.pipe';
import { OrderBy } from './pipes/order-by.pipe';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  observer: true,
  direction: 'horizontal',
  slidesPerView: 1,
  keyboard: true,
  mousewheel: true,
  scrollbar: false,
  navigation: true,
  pagination: true,
  threshold: 0,
  autoplay: {
    delay: 10000
  }
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SliderComponent,
    FeaturedProductComponent,
    FeaturedSpecsComponent,
    ProductsBySizeComponent,
    HeaderComponent,
    LanguagesComponent,
    FooterComponent,
    ProductComponent,
    FeaturesComponent,
    SpecsComponent,
    ProductSupportComponent,
    CategoryComponent,
    MobilNavigationComponent,
    HitachiGlobalMenuComponent,
    MainNavigationComponent,
    LanguageBarComponent,
    ImagesComponent,
    HighlightedTextComponent,
    BigComponent,
    SmallComponent,
    CapitalizePipes,
    FilterPipe,
    OrderBy
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BreadcrumbsModule,
    HttpClientModule,
    SwiperModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: StaticTextLoader
      }
    }),
    ResponsiveModule.forRoot(),
    SlickModule.forRoot()
  ],
  providers: [
    ProductService,
    CategoryService,
    LocationService,
    SocialMediaService,
    FeaturedProductService,
    FeaturedSpecsService,
    SlidesService,
    SimpleGlobal,
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
