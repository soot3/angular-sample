import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoryComponent } from './category/category.component';
import { ProductComponent } from './product/product.component';
import { FeaturesComponent } from './product/product-detail/features/features.component';
import { SpecsComponent } from './product/product-detail/specs/specs.component';
import { ProductSupportComponent } from './product/product-detail/product-support/product-support.component';
import { HomeComponent } from './home/home.component';
import { LanguagesComponent } from './common/languages/languages.component';
// import { AppPreloadingStrategy } from './shared/app-preloading.strategy';
import { AppCustomPreloader } from './app-routing-loader';

const appRoutes: Routes = [
  // { path: '', redirectTo: '/eu/en/', pathMatch: 'full' },
  { path: ':country/:language', component: HomeComponent, data: { preload: true } },
  { path: ':country/:language/languages', component: LanguagesComponent, data: { preload: true } },
  { path: ':country/:language/category/:id/:link', component: CategoryComponent, data: { preload: true } },
  {
    path: ':country/:language/product/:id/:link', component: ProductComponent, data: { preload: true }, children: [
      { path: '', redirectTo: 'features', pathMatch: 'full' },
      { path: 'features', component: FeaturesComponent, data: { preload: true } },
      { path: 'specifications', component: SpecsComponent, data: { preload: true } },
      { path: 'support', component: ProductSupportComponent, data: { preload: true } }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes,
    { preloadingStrategy: AppCustomPreloader } )],
  exports: [RouterModule],
  providers: [AppCustomPreloader]
})

export class AppRoutingModule {

}
