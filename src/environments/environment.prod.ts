export const environment = {
  production: true,
  defaultCountry: 'eu',
  rootUrl: 'http://34.244.173.118/',
  videoUrl:  'https://www.youtube.com/embed',
  siteTitle: 'Hitachi Televisions',
  socialMediaElements: ''
};
