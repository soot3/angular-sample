# Ismet Yavuz Angular Sample Project
This project was generated with Angular CLI version 6.0.8.

# Instructions
- Run ng serve for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.
- You must use "Allow-Control-Allow-Origin: *" Google Chrome extension to view api. (https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?hl=en)
